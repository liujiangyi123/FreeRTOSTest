#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "timer.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "tast_test.h"
#include "tast_test2.h"
#include "queue_test.h"
#include "list_test.h"
#include "mpu_wrappers.h"
#include "ui.h"


int main ( void )
{
    NVIC_PriorityGroupConfig ( NVIC_PriorityGroup_4 ); //设置系统中断优先级分组4
    delay_init();	    				//延时函数初始化
    uart_init (2000000);					//初始化串口
    ui_init();

    printf ( "power on\n" );

    /* vTaskStartScheduler();          //开启任务调度 */
    /* portENABLE_INTERRUPTS(); */


    while (1)
    {
	if ( IS_KEY0_ON())
	    LED0_ON();
	else
	    LED0_OFF();

	if ( IS_KEY1_ON())
	    LED1_ON();
	else
	    LED1_OFF();
    }
}




