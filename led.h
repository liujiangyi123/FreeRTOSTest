#ifndef __LED_H
#define __LED_H	 

#include "sys.h"
#include <stdbool.h>

#define LED_RED		PBout(5)// PB5
#define LED_YELLOW	PEout(5)// PE5	

void LED_Init(void);    //��ʼ��

#define TEST1_GPIO_PORT   GPIOA
#define TEST1_GPIO_PIN    GPIO_Pin_1

#define TEST1_LOW()	GPIO_ResetBits(TEST1_GPIO_PORT,TEST1_GPIO_PIN)
#define TEST1_HIGH()	GPIO_SetBits(TEST1_GPIO_PORT,TEST1_GPIO_PIN)

#define LED_RED_GPIO_PORT   GPIOB
#define LED_RED_GPIO_PIN    GPIO_Pin_5

#define LED_YELLOW_GPIO_PORT   GPIOE
#define LED_YELLOW_GPIO_PIN    GPIO_Pin_5

#define LED_Y_ON()   GPIO_ResetBits(LED_YELLOW_GPIO_PORT,LED_YELLOW_GPIO_PIN)
#define LED_Y_OFF()  GPIO_SetBits(LED_YELLOW_GPIO_PORT,LED_YELLOW_GPIO_PIN)

#define LED_R_ON()   GPIO_ResetBits(LED_RED_GPIO_PORT,LED_RED_GPIO_PIN)
#define LED_R_OFF()  GPIO_SetBits(LED_RED_GPIO_PORT,LED_RED_GPIO_PIN)
		 				    
void test_init(void);
#endif
