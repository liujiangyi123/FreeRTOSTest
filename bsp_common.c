#include "bsp_common.h"

void bsp_common_gpio_init( GPIO_TypeDef *GPIOx, uint32_t GPIO_Pin, GPIOMode_TypeDef gpio_mode)

{
    GPIO_InitTypeDef io_init;
    io_init.GPIO_Pin = GPIO_Pin;				 //LED_RED-->PB.5 端口配置
    io_init.GPIO_Mode = gpio_mode; 		 //推挽输出
    io_init.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz

    GPIO_Init( GPIOx, &io_init );					 //根据设定参数初始化GPIOB.5
}

