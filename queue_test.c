#include "queue_test.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

xQueueHandle xQueue;

typedef struct
{
    uint8_t senderID;
    uint8_t data;
} MsgType;

void queue_task_add_item( void* pvParameters )
{
    MsgType msg;

    while ( 1 )
    {
        msg.senderID = 1;
        msg.data = 0x01;
        xQueueSend( xQueue, &msg, 100 );
	vTaskDelay(1000);
    }
}

void queue_task_add_item2( void* pvParameters )
{
    MsgType msg;

    while ( 1 )
    {
        msg.senderID = 2;
        msg.data = 0x02;
        xQueueSend( xQueue, &msg, 100 );
	vTaskDelay(1000);
    }
}


void queue_task_recv_item( void* pvParameters )
{

    MsgType msg;
    while ( 1 )
    {
        if ( uxQueueMessagesWaiting( xQueue ) )
        {
            xQueueReceive( xQueue, &msg, 100 );
	    if ( msg.senderID  == 1)
		printf("user1 send:%d\n",msg.data);
	    else
		printf("user2 send:%d\n",msg.data);
        }

    }
}

void queue_test_create_task( void )
{
    xQueue = xQueueCreate( 10, 2 );

    if ( xQueue != NULL )
    {
        xTaskCreate ( queue_task_add_item, "queue_task_add_item", 128, NULL, 1, NULL );
        xTaskCreate ( queue_task_add_item2, "queue_task_add_item2", 128, NULL, 1, NULL );
        xTaskCreate ( queue_task_recv_item, "queue_task_recv_item", 128, NULL, 1, NULL );
    }
}

