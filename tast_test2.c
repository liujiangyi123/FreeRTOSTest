#include "tast_test2.h"
#include "FreeRTOS.h"
#include "task.h"
#include "led.h"

void task3(void* pvParameters)
{
    uint32_t i = 0;
    while ( 1 )
    {
	printf( "%d,task3...\n",i );
	vTaskDelay(100);
    }
}

void task4(void* pvParameters)
{
    uint32_t i = 0;

    while ( 1 )
    {
	printf( "%d,task4...\n",i );
	vTaskDelay(100);
    }
}

void vApplicationIdleHook(void)
{
    static bool io_stat = false;
    while (1)
    {
	if ( io_stat)
	    TEST1_HIGH();
	else
	    TEST1_LOW();
	io_stat = !io_stat;
    }
}

void task_test2_start_task( void )
{
    xTaskCreate( task3, "task3", 128, NULL, 2, NULL );
    xTaskCreate( task4, "task4", 128, NULL, 2, NULL );
}
