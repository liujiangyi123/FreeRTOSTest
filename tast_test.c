#include "tast_test.h"
#include "FreeRTOS.h"
#include "task.h"

//任务优先级
#define START_TASK_PRIO		1
//任务堆栈大小
#define START_STK_SIZE 		128
//任务句柄
TaskHandle_t StartTask_Handler;

//任务优先级
#define TASK1_TASK_PRIO		5
//任务堆栈大小
#define TASK1_STK_SIZE 		128
//任务句柄
TaskHandle_t Task1Task_Handler;

//任务优先级
#define TASK2_TASK_PRIO		4
//任务堆栈大小
#define TASK2_STK_SIZE 		128
//任务句柄
TaskHandle_t Task2Task_Handler;

//task1任务函数
void task1_task ( void *pvParameters )
{
    static uint8_t tast1_val = 5;
    while ( 1 )
    {
        printf( "task1 start...\n" );
	printf("tast1 tast1_val = %d\n",tast1_val );
        vTaskDelay ( 1000 );
        printf( "task1 end ...\n" );
        vTaskDelay ( 1000 );
    }
}

//task2任务函数
void task2_task ( void *pvParameters )
{
    static uint8_t tast1_val = 5;
    while ( 1 )
    {
        printf( "task2 start...\n" );
	tast1_val = 6;

	printf("tast2 tast1_val = %d\n",tast1_val );

        vTaskDelay ( 1500 );
        printf( "task2 end ...\n" );
        vTaskDelay ( 1500 );
    }
}

//开始任务任务函数
void start_task ( void *pvParameters )
{
    taskENTER_CRITICAL();           //进入临界区

    //创建TASK1任务
    xTaskCreate ( ( TaskFunction_t ) task1_task,
                  ( const char*    ) "task1_task",
                  ( uint16_t       ) TASK1_STK_SIZE,
                  ( void*          ) NULL,
                  ( UBaseType_t    ) TASK1_TASK_PRIO,
                  ( TaskHandle_t*  ) &Task1Task_Handler );

    //创建TASK2任务
    xTaskCreate ( ( TaskFunction_t ) task2_task,
                  ( const char*    ) "task2_task",
                  ( uint16_t       ) TASK2_STK_SIZE,
                  ( void*          ) NULL,
                  ( UBaseType_t    ) TASK2_TASK_PRIO,
                  ( TaskHandle_t*  ) &Task2Task_Handler );

    vTaskDelete ( StartTask_Handler ); //删除开始任务

    taskEXIT_CRITICAL();            //退出临界区
}

void tast_test_create_start_task( void )
{
    xTaskCreate ( ( TaskFunction_t ) start_task,        //任务函数
                  ( const char*    ) "start_task",        //任务名称
                  ( uint16_t       ) START_STK_SIZE,      //任务堆栈大小
                  ( void*          ) NULL,                //传递给任务函数的参数
                  ( UBaseType_t    ) START_TASK_PRIO,     //任务优先级
                  ( TaskHandle_t*  ) &StartTask_Handler ); //任务句柄

}
