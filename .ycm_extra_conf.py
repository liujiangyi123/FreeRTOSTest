import os
import ycm_core

flags = [
  '-Wall',
  '-Wextra',
  '-Werror',
  '-Wno-long-long',
  '-Wno-variadic-macros',
  '-fexceptions',
  '-ferror-limit=10000',
  '-DNDEBUG',
  '-std=c89',
  '-xc',
  '-isystem/usr/include/',
  '-isystem/mnt/e/code/MyCode/FreeRtos/include/',
  '-isystem/mnt/e/code/MyCode/FreeRtos/portable/',
  '-isystem/mnt/e/code/MyCode/FreeRTOSTest/STM32F10x_FWLib/inc/',
  '-isystem/mnt/e/code/MyCode/FreeRTOSTest/STM32F10x_FWLib/src/',
  ]

SOURCE_EXTENSIONS = [ '.cpp', '.cxx', '.cc', '.c', ]

def FlagsForFile( filename, **kwargs ):
  return {
  'flags': flags,
  'do_cache': True
  }
