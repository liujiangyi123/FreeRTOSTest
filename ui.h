#ifndef _UI_H_
#define _UI_H_

#define KEY0_GPIO_PORT	    GPIOE
#define KEY0_GPIO_PIN	    GPIO_Pin_4
#define IS_KEY0_ON()	    (Bit_RESET == GPIO_ReadInputDataBit(KEY0_GPIO_PORT,KEY0_GPIO_PIN))

#define KEY1_GPIO_PORT	    GPIOE
#define KEY1_GPIO_PIN	    GPIO_Pin_3
#define IS_KEY1_ON()	    (Bit_RESET == GPIO_ReadInputDataBit(KEY1_GPIO_PORT,KEY1_GPIO_PIN))

#define LED0_GPIO_PORT	    GPIOE
#define LED0_GPIO_PIN	    GPIO_Pin_5
#define LED0_ON()	    GPIO_ResetBits(LED0_GPIO_PORT,LED0_GPIO_PIN)
#define LED0_OFF()	    GPIO_SetBits(LED0_GPIO_PORT,LED0_GPIO_PIN)

#define LED1_GPIO_PORT	    GPIOB
#define LED1_GPIO_PIN	    GPIO_Pin_5
#define LED1_ON()	    GPIO_ResetBits(LED1_GPIO_PORT,LED1_GPIO_PIN)
#define LED1_OFF()	    GPIO_SetBits(LED1_GPIO_PORT,LED1_GPIO_PIN)

void ui_init( void );
#endif
