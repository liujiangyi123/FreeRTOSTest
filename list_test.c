#include "list_test.h"
#include "FreeRTOS.h"
#include "task.h"
#include "list.h"
#include "led.h"

void list_test_led_y( void* pvParameters )
{
    while ( 1 )
    {
        LED_Y_ON();
        vTaskDelay( 500 );
        LED_Y_OFF();
        vTaskDelay( 500 );
    }

}

void list_test_led_r( void* pvParameters )
{
    List_t testList;
    ListItem_t list_item_1;
    ListItem_t list_item_2;
    ListItem_t list_item_3;

    vListInitialise( &testList );
    vListInitialiseItem( &list_item_1 );
    vListInitialiseItem( &list_item_2 );
    vListInitialiseItem( &list_item_3 );

    list_item_1.xItemValue = 0x40;
    list_item_2.xItemValue = 0x50;
    list_item_3.xItemValue = 0x70;

    printf( "1 val:0x%x\n", testList.xListEnd.xItemValue );

    vListInsert( &testList, &list_item_1 );
    printf( "2 val:0x%x\n", testList.xListEnd.pxNext->xItemValue);

    vListInsert( &testList, &list_item_2 );
    printf( "3 val:0x%x\n", testList.xListEnd.pxNext->pxNext->xItemValue);
    vListInsert( &testList, &list_item_3 );
    printf( "4 val:0x%x\n", testList.xListEnd.pxNext->pxNext->pxNext->xItemValue);
    printf( "5 val:0x%x\n", testList.xListEnd.pxNext->pxNext->pxNext->pxNext->xItemValue);

    while ( 1 )
    {
        LED_R_ON();
        vTaskDelay( 1000 );
        LED_R_OFF();
        vTaskDelay( 1000 );
    }
}

void list_test_start_task( void )
{
    xTaskCreate ( list_test_led_y, "list_test_led_y", 128, NULL, 1, NULL );
    xTaskCreate ( list_test_led_r, "list_test_led_r", 128, NULL, 1, NULL );
}
