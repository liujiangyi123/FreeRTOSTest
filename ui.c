#include "ui.h"
#include "stm32f10x_gpio.h"
#include "bsp_common.h"
#include "stm32f10x_rcc.h"


void ui_init( void )
{
    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE );
    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOE, ENABLE );

    bsp_common_gpio_init( KEY0_GPIO_PORT, KEY0_GPIO_PIN, GPIO_Mode_IPU );
    bsp_common_gpio_init( KEY1_GPIO_PORT, KEY1_GPIO_PIN, GPIO_Mode_IPU );

    bsp_common_gpio_init( LED0_GPIO_PORT, LED0_GPIO_PIN, GPIO_Mode_Out_PP );
    bsp_common_gpio_init( LED1_GPIO_PORT, LED1_GPIO_PIN, GPIO_Mode_Out_PP );

}


void ui_process( void )
{

}
