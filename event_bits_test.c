#include "event_bits_test.h"
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "ui.h"

static TaskHandle_t startTaskHandler;
static TaskHandle_t eventSetBitsTaskHandler;
static TaskHandle_t eventGrpTaskHandler;
static TaskHandle_t eventQueryTaskHandler;

EventGroupHandle_t eventGroupHandler;

#define EVENTBIT_0  (1 << 0)
#define EVENTBIT_1  (1 << 1)
#define EVENTBIT_2  (1 << 2)
#define EVENTBIT_ALL  ( EVENTBIT_0 | EVENTBIT_1 | EVENTBIT_2)



static void event_set_bits_task( void* pvParameters )
{

}

static void event_grp_task( void* pvParameters )
{

}

static void event_query_task( void* pvParameters )
{

}



static void event_bits_start_task( void* pvParameters )
{
    taskENTER_CRITICAL();
    eventGroupHandler = xEventGroupCreate();
    

    xTaskCreate ( event_set_bits_task, "event_set_bits_task", 128, NULL, 2, eventSetBitsTaskHandler);
    xTaskCreate ( event_grp_task, "event_grp_task", 128, NULL, 2, eventGrpTaskHandler);
    xTaskCreate ( event_query_task, "event_query_task", 128, NULL, 2, eventQueryTaskHandler);

    vTaskDelete(startTaskHandler);

    taskEXIT_CRITICAL();

}

void event_bits_init( void )
{
    xTaskCreate ( event_bits_start_task, "event_bits_start_task", 128, NULL, 1, startTaskHandler );
    vTaskStartScheduler();
}
