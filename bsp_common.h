#ifndef _BSP_COMMON_H_
#define _BSP_COMMON_H_

#include "stm32f10x.h"
#include <stdint.h>
#include <stdbool.h>

void bsp_common_gpio_init( GPIO_TypeDef *GPIOx, uint32_t GPIO_Pin, GPIOMode_TypeDef gpio_mode);

#endif
